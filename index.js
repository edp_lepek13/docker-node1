const express = require('express')
const app = express()
const http = require('http')

app.get('/', (req, res) => {
    return res.json({
        status: true,
        message: "ok"
    })
})

// express.

const server = http.Server(app)


server.listen(3000, () => {
    console.log('port 3000');
})